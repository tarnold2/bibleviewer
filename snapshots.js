/** Created by Terry Arnold II */
/*jslint indent: 4, maxlen: 100, nomen: true, es5: true */
/*global require, module */
/*eslint no-console: ["error", { allow: ["warn","log"] }] */

//.js
(function () {
    'use strict';
    
    var fs = require('fs'),
        common = require('./common')(),
        _snapShotNumber = 0,
        saveScreenShotFile,
        saveSourceShotFile,
        takeSnapShot,
        logErrorAndTakeSnapShot,
        takeOneAnd;
    
    saveScreenShotFile = function (image, err) {
        if (!err) {
            common.logger.info("Taking screen shot number " + _snapShotNumber);
            fs.writeFile(common.outputBaseDir + "/overseer-" +
                             common.timestamp + '-' + _snapShotNumber +
                             '.png', image, common.IMAGE_ENCODING,
                             common.logError);
        } else {
            common.logger.error(err);
        }
    };
    
    saveSourceShotFile = function (pageSource) {
        common.logger.info("Saving page source shot number " + _snapShotNumber);
        fs.writeFile(common.outputBaseDir + "/overseer-" +
                         common.timestamp + '-' + _snapShotNumber +
                             '.html', pageSource, common.TEXT_ENCODING,
                             common.logError);
    };
    
    takeSnapShot = function () {
        _snapShotNumber += 1;
        common.driver.takeScreenshot()
            .then(saveScreenShotFile)
            .catch(common.logError);
        
        common.driver.getPageSource()
            .then(saveSourceShotFile)
            .catch(common.logError);
    };
    
    logErrorAndTakeSnapShot = function (err) {
        if (!common.shouldExit) {
            common.logError(err);
            takeSnapShot();
        }
    };
    
    takeOneAnd = function (next, timeToWait) {
        timeToWait = (timeToWait || common.MEDIUM_WAIT);
        return function () {
            //There is no catch clause on this because it is intended
            //to be thrown to the calling function since this is normally
            //used in a promise chain
            //That is also why we are returning this as a promise chain
            return common.driver
                .sleep(timeToWait)
                .then(takeSnapShot)
                .then(next);
        };
    };
 
    module.exports = {
        "takeSnapShot" : takeSnapShot,
        "logErrorAndTakeSnapShot" : logErrorAndTakeSnapShot,
        "saveScreenShotFile" : saveScreenShotFile,
        "saveSourceShotFile" : saveSourceShotFile,
        "takeOneAnd" : takeOneAnd
    };
    
}());