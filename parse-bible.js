/** Created by Terry Arnold II */
/*jslint indent: 4, maxlen: 100, nomen: true, es5: true */
/*global require, process */
/*eslint no-console: ["error", { allow: ["warn","log"] }] */

//parse-bible.js
(function () {
    'use strict';

    var common = require('./common')(),
        readline = require('readline'),
        snapshots = require('./snapshots'),
        webdriver = require('selenium-webdriver'),
        _ = require('underscore'),
        By = webdriver.By,
        initialize,
        findBreadcrumbsDiv,
        parseBreadcrumbsText,
        parseScripturalReference,
        goToNextVerse,
        printErrorAndExit,
        safeExit;

    safeExit = function (context) {
        common.logger.debug("Context: " + context);
        common.requestExit();
        common.emitter.emit(common.LOOSE_JOBS_FINISHED);
    };
    
    printErrorAndExit = function (context) {
        return function (err) {
            common.logger.error(err);
            safeExit(context);
        };
    };
    
/* Main Thread Processing */
    findBreadcrumbsDiv = function (context) {
        return function () {
            common.logger.info("Reading the Verse...");
            common.driver.findElements(By.xpath(common.BREADCRUMBS_XPATH))
                .then(function (elements) {
                    if (elements.length > 0) {
                        elements[0]
                            .getText()
                            .then(parseBreadcrumbsText(context))
                            .then(common.shortWaitAndThen(goToNextVerse(context)))
                            .catch(printErrorAndExit(context));
                    }
                })
                .catch(printErrorAndExit(context));
        };
    };
    
    parseBreadcrumbsText = function (context) {
        return function (breadcrumbsText) {
            var verse = {};
            
            common.logger.info("Parsing Breadcrumbs Text: " + breadcrumbsText);
            verse.breadcrumbsArray = breadcrumbsText.split(" > ");
            verse.canon = verse.breadcrumbsArray[0];
            verse.language = verse.breadcrumbsArray[1];
            verse.scripturalReference = verse.breadcrumbsArray[2];
            parseScripturalReference(verse);
            
            common.logger.info(verse);
            
            context.verses.push(verse);
        };
    };
    
    parseScripturalReference = function (scripture) {
        scripture.scripturalReferenceArray = scripture.scripturalReference.split(/[ :]/);
        scripture.book = "";

        //parse scriptural reference
        _.forEach(scripture.scripturalReferenceArray, function (piece, i, arr) {
            if (i === arr.length - 1) {
                scripture.verse = piece;
            } else if (i === arr.length - 2) {
                scripture.chapter = piece;
            } else {
                if (scripture.book !== "") {
                    scripture.book += " ";
                }
                scripture.book += piece;
            }
        });
    };
    
    goToNextVerse = function (context) {
        return function () {
            common.driver
                .findElements(By.xpath(common.NEXT_VERSE_LINK_XPATH))
                .then(function (linksToClick) {
                    if (context.scripturalReference === common.LAST_VERSE) {
                        safeExit(context);
                    } else if (linksToClick.length > 0) {
                        linksToClick[0]
                            .click()
                            .then(snapshots.takeOneAnd(findBreadcrumbsDiv(context),
                                                       common.SHORT_WAIT))
                            .catch(printErrorAndExit(context));
                    } else {
                        printErrorAndExit(context)("Could not go to next verse");
                    }
                })
                .catch(printErrorAndExit(context));
        };
    };
    
    initialize = function () {
        var context = {
            "verses" : []
        };
        
        //Watch for keyboard input
        readline.emitKeypressEvents(process.stdin);
        process.stdin.setRawMode(true);
        process.stdin.on('keypress', function (str, key) {
            if (key.ctrl && key.name === 'c') {
                common.logger.info("You pressed CTRL+C...");
                safeExit();
            }
        });

        // catches "kill pid" (for example: nodemon restart)
        process.on('SIGUSR1', common.requestExit);
        process.on('SIGUSR2', common.requestExit);

        //catches uncaught exceptions
        process.on('uncaughtException', printErrorAndExit(context));
        
        common.logger.info("Bible Viewer");
        common.logger.info("========================================");
        
        common.driver
            .get(common.HOME_VERSE_PAGE)
            .then(snapshots.takeOneAnd(findBreadcrumbsDiv(context), common.SHORT_WAIT))
            .catch(printErrorAndExit(context));
        
    };
    
    initialize();
    
}());