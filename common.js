/** Created by Terry Arnold II */
/*jslint indent: 4, maxlen: 100, nomen: true, es5: true */
/*global require, module, process, setTimeout, console */
/*eslint no-console: 0 */

//common.js
(function () {
    'use strict';
    
        //Libraries
    var fs = require('fs'),
        log4js = require('log4js'),
        webdriver = require('selenium-webdriver'),
        firefox = require('selenium-webdriver/firefox'),
        EventEmitter = require('events').EventEmitter,
        timestampUtil = require('./timestamp-util'),
        constants = require('./constants'),
        _ = require('underscore'),
        rOptions,
        //Constants
        By = webdriver.By,
        fullyResolved = webdriver.promise.fullyResolved,
        promise = webdriver.promise,
        //Private Member Variables
        _timestamp = timestampUtil.baseTimestamp,
        _options = new firefox.Options(),
        _emitter = new EventEmitter(),
        _numOfExitTries = 0,
        _shouldExit = false,
        _outputBaseDir,
        _driver,
        _logger,
        //Functions
        findElementByText,
        findElementsByText,
        waitAndThen,
        shortWaitAndThen,
        longWaitAndThen,
        setupSeleniumDriver,
        shutdownDriver,
        setShouldExit,
        requestExit,
        quit,
        logAndQuit,
        logError,
        checkForPageNotFound,
        init,
        shouldExit,
        sayNoPageNotFoundError,
        throwPageNotFound;
        

/* Common Functions */
    
    findElementByText = function (elementClause, searchText) {
        var promiseArray = [],
            _retries = 0,
            _elements,
            retry,
            lookThroughElements,
            matchText;
        
        retry = function (err) {
            if (_retries < rOptions.retries) {
                _retries += 1;
                
                //Print out the Error if its not a Stale Element Error
                //Stale element errors happen when the page gets refreshed
                //and can be largely be ignored with a retry
                if (err.name === constants.STALE_ELEMENT_ERROR) {
                    _logger.debug(err);
                    
                    return _driver
                        .findElements(elementClause)
                        .then(lookThroughElements)
                        .catch(retry);
                } else if (err.name === constants.NO_SUCH_SESSION_ERROR) {
                    _logger.error("Browser window appears to be closed... shutting down", err);
                    //We should exit everything if this happens
                    shutdownDriver();
                } else {
                    _logger.error(err);
                    _logger.info("Retrying ...");
                    
                    return _driver
                        .findElements(elementClause)
                        .then(lookThroughElements)
                        .catch(retry);
                }
            }
        };
        
        lookThroughElements = function (elements) {
            var i;
            _logger.debug("In findElementByText:lookThroughElements");
            _elements = elements;
            
            if (elements.length === 0) {
                return null;
            } else {
                _logger.debug("Finding element with text '" + searchText +
                              "' among " + elements.length + " item(s) ...");
                for (i = 0; i < elements.length; i += 1) {
                    promiseArray.push(elements[i].getText().catch(retry));
                }
                
                return fullyResolved(promiseArray)
                    .then(matchText)
                    .catch(retry);
            }
        };

        matchText = function (elementTextArray) {
            var i;
            _logger.debug("In findElementByText:matchText");
            for (i = 0; i < elementTextArray.length; i += 1) {
                if (elementTextArray[i] === searchText) {
                    return _elements[i];
                }
            }
            return null;
        };
        
        return _driver
            .findElements(elementClause)
            .then(lookThroughElements)
            .catch(retry);
    };
    
    findElementsByText = function (elementClause, searchText) {
        var promiseArray = [],
            _retries = 0,
            _elements,
            retry,
            lookThroughElements,
            matchText;

        retry = function (err) {
            if (_retries < rOptions.retries) {
                _retries += 1;
                
                //Print out the Error if its not a Stale Element Error
                //Stale element errors happen when the page gets refreshed
                //and can be largely be ignored with a retry
                if (err.name === constants.STALE_ELEMENT_ERROR) {
                    _logger.debug(err);
                    //Retry stale elements
                    return _driver
                        .findElements(elementClause)
                        .then(lookThroughElements)
                        .catch(retry);
                } else if (err.name === constants.NO_SUCH_SESSION_ERROR) {
                    _logger.error("Browser window appears to be closed... shutting down", err);
                    //We should exit everything if this happens
                    shutdownDriver();
                } else {
                    _logger.error(err);
                    _logger.info("Retrying ...");
                    
                    return _driver
                        .findElements(elementClause)
                        .then(lookThroughElements)
                        .catch(retry);
                }
            }
        };
        
        lookThroughElements = function (elements) {
            var i;
            
            _logger.debug("In findElementsByText:lookThroughElements");
            _elements = elements;
            
            if (elements.length === 0) {
                return [];
            } else {
                _logger.debug("Finding elements with text '" + searchText + "'...");
                for (i = 0; i < elements.length; i += 1) {
                    promiseArray.push(elements[i].getText().catch(retry));
                }
                
                return fullyResolved(promiseArray)
                    .then(matchText)
                    .catch(retry);
            }
        };

        matchText = function (elementTextArray) {
            var elementsFound = [],
                i;
            _logger.debug("In findElementsByText:matchText");
            for (i = 0; i < elementTextArray.length; i += 1) {
                if (elementTextArray[i] === searchText) {
                    elementsFound.push(_elements[i]);
                }
            }
            return elementsFound;
        };
        
        return _driver
            .findElements(elementClause)
            .then(lookThroughElements)
            .catch(retry);
    };
    
    waitAndThen = function (milliseconds, next) {
        return function () {
            _driver
                .sleep(milliseconds)
                .then(next);
        };
    };
    
    shortWaitAndThen = function (next) {
        return waitAndThen(constants.SHORT_WAIT, next);
    };
    
    longWaitAndThen = function (next) {
        return waitAndThen(constants.LONG_WAIT, next);
    };
    
/* Selenium Functions */
    setupSeleniumDriver = function () {
        if (!_driver) {
            _logger.info("Setting up Selenium Driver...");
            _options.addArguments("-headless", "--window-size=" + rOptions.windowWidth +
                                 "," + rOptions.windowHeight);

            _options.setPreference("browser.startup.homepage", "about:blank");
            _options.setPreference("browser.startup.homepage_override.mstone", "ignore");
            _options.setPreference("startup.homepage_welcome_url", "about:blank");
            _options.setPreference("startup.homepage_welcome_url.additional",
                                  "about:blank");

            _driver = new webdriver.Builder()
                .forBrowser('firefox')
                .setFirefoxOptions(_options)
                .build();
        }
    };
    
    shutdownDriver = function () {
        _logger.info("Shutting down selenium driver...");
        _driver.quit();
        
        setTimeout(process.exit, constants.LONG_WAIT);
    };
    
/* Quit Functions */
    shouldExit = function () {
        return _shouldExit;
    };
    
    setShouldExit = function (shouldExit) {
        _shouldExit = shouldExit;
    };
    
    requestExit = function (err) {
        if (!_shouldExit) {
            _logger.info("Exit Requested...Please wait a moment " +
                          "for proper shutdown");
            if (err) {
                _logger.error(err);
            }
            quit();
        }
    };
    
    quit = function () {
        if (_numOfExitTries === 0) {
            _shouldExit = true;
            _emitter.once(constants.LOOSE_JOBS_FINISHED, shutdownDriver);
        } else {
            _logger
                .warn("Quit Called but was not logged in or have tried to exit before already");
            _logger.debug("Exit Try: " + _numOfExitTries);
        }
        _numOfExitTries += 1;
    };
    
    logAndQuit = function (err) {
        _logger.info("LogAndQuit called");
        logError(err);
        requestExit();
    };

/* Error Handling */
    
    logError = function (err) {
        if (err && !_shouldExit) {
            _logger.error("Log Error: " + err);
        }
    };
    
    sayNoPageNotFoundError = function (context) {
        _logger.debug("No page not found error detected...");
        if (context) {
            if (context.emitter) {
                context.emitter.emit(constants.THERE_IS_NO_PAGE_NOT_FOUND_ERROR, context);
            } else {
                _emitter.emit(constants.THERE_IS_NO_PAGE_NOT_FOUND_ERROR, context);
            }
        } else {
            _emitter.emit(constants.THERE_IS_NO_PAGE_NOT_FOUND_ERROR);
        }
    };
    
    throwPageNotFound = function (context) {
        return function () {
            if (context) {
                context.error = "Page not found error";
                if (context.emitter) {
                    context.emitter.emit(constants.PAGE_NOT_FOUND, context);
                } else {
                    _emitter.emit(constants.PAGE_NOT_FOUND, context);
                }
            } else {
                _emitter.emit(constants.PAGE_NOT_FOUND, {
                    "error" : "Page not found error"
                });
            }
        };
    };
    
    checkForPageNotFound = function (context) {
        return function () {
            var checkErrorType = function (err) {
                if (err.name === constants.NO_SUCH_ELEMENT_ERROR) {
                    sayNoPageNotFoundError(context);
                } else {
                    _logger.error(err);
                    shutdownDriver();
                }
            };

            //If you find a http 404 element
            _driver.findElement(By.id("http_404_title"))
                .then(throwPageNotFound(context))//Then report the error
                .catch(checkErrorType);//Otherwise evaluate error
        };
    };
    
    
    
    //Initialization Steps
    init = function () {
        var commandLineConfigLocation = process.argv[2],
            configString,
            loggingConfig;
        
        //Read config file
        if (fs.existsSync(commandLineConfigLocation)) {
            console.log("Reading configuration from " + commandLineConfigLocation);
        } else if (fs.existsSync(constants.DEFAULT_CONFIG_LOCATION)) {
            commandLineConfigLocation = constants.DEFAULT_CONFIG_LOCATION;
        } else {
            console.error("Usage: node overseer <config-file>");
            console.log("Default config: config.json");
            process.exit(1);
        }
        
        try {
            configString = fs.readFileSync(commandLineConfigLocation, constants.TEXT_ENCODING)
                .toString();
            
            rOptions = JSON.parse(configString);
            
            //Setup output base directory with timestamp
            _outputBaseDir = rOptions.outputBaseDir + "/bibleviewer-" + _timestamp;

            //Create Output Base Directory if it does not exist
            if (!fs.existsSync(_outputBaseDir)) {
                fs.mkdirSync(_outputBaseDir, { recursive: true });
            }

            //Setup Logger
            loggingConfig = rOptions.logging;
            loggingConfig.appenders.app.filename = _outputBaseDir + "/" +
                loggingConfig.appenders.app.filename;
            log4js.configure(loggingConfig);

            _logger = log4js.getLogger();
            _logger.debug("Logger started");
            
        } catch (err) {
            console.error(err);
            process.exit(1);
        }
    };
    
    init();
    
    module.exports = function (disableSelenium) {
        if (!disableSelenium) {
            setupSeleniumDriver();
        }

        return _.extend({},
            {
                "rOptions" : rOptions,
                "outputBaseDir" : _outputBaseDir,
                "timestamp" : _timestamp,
                "findElementByText" : findElementByText,
                "findElementsByText" : findElementsByText,
                "shouldExit" : shouldExit,
                "promise" : promise,
                "driver" : _driver,
                "logger" : _logger,
                "emitter" : _emitter,
                "quit" : quit,
                "fullyResolved" : fullyResolved,
                "setShouldExit" : setShouldExit,
                "requestExit" : requestExit,
                "waitAndThen" : waitAndThen,
                "shortWaitAndThen" : shortWaitAndThen,
                "longWaitAndThen" : longWaitAndThen,
                "logAndQuit" : logAndQuit,
                "logError" : logError,
                "checkForPageNotFound" : checkForPageNotFound,
                "setupSeleniumDriver" : setupSeleniumDriver
            },
            constants);
    };
    
}());