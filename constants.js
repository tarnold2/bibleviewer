/** Created by Terry Arnold II */
/*jslint indent: 4, maxlen: 100, nomen: true, es5: true */
/*global module */
/*eslint no-console: ["error", { allow: ["warn","log"] }] */

//constants.js
(function () {
    'use strict';
    
    module.exports = {
        CONNECTION_REFUSED_ERROR : "ECONNREFUSED",
        NO_SUCH_ELEMENT_ERROR : "NoSuchElementError",
        NO_SUCH_SESSION_ERROR : "NoSuchSessionError",
        STALE_ELEMENT_ERROR : "StaleElementReferenceError",
        IMAGE_ENCODING : "base64",
        TEXT_ENCODING : "utf-8",
        DEFAULT_CONFIG_LOCATION : './config.json',
        HOME_VERSE_PAGE : 'https://biblehub.com/text/genesis/1-1.htm',
        LAST_VERSE : 'Revelation 22:21',
        BREADCRUMBS_XPATH : "//div[@id='breadcrumbs']",
        VERSE_TEXT_XPATH : "//*[contains(text(),'Strong')]/following-sibling::td/../following-sibling::tr//td[@class='hebrew2' or @class='greek2']",
        MORPHOLOGY_TEXT_XPATH : "//*[contains(text(),'Strong')]/following-sibling::td/../following-sibling::tr//td[@class='pos']",
        ENGLISH_TEXT_XPATH : "//*[contains(text(),'Strong')]/following-sibling::td/../following-sibling::tr//td[@class='eng']",
        STRONGS_NUMBER_XPATH : "//*[contains(text(),'Strong')]/following-sibling::td/../following-sibling::tr//td[@class='strongsnt']",
        NEXT_VERSE_LINK_XPATH : "//div[@id='topheading']/a[2]"
    };
}());