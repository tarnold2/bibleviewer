/** Created by Terry Arnold II */
/*jslint indent: 4, maxlen: 100, nomen: true, es5: true */
/*global module */
/*eslint no-console: ["error", { allow: ["warn","log"] }] */

//timestamp-util.js
(function () {
    'use strict';
    
        //Functions
    var pad2,
        getTimestampString,
        //Private member variables
        _timestamp = new Date(),
        _baseTimestamp;
    
    pad2 = function (n) {
        return n < 10 ? '0' + n : n;
    };
    
    getTimestampString = function (timestamp) {
        return timestamp.getFullYear().toString() + pad2(timestamp.getMonth() + 1) +
                pad2(timestamp.getDate()) + pad2(timestamp.getHours()) +
                pad2(timestamp.getMinutes()) + pad2(timestamp.getSeconds());
    };
    
    _baseTimestamp = getTimestampString(_timestamp);
    
    module.exports = {
        "getTimestampString" : getTimestampString,
        "baseTimestamp" : _baseTimestamp
    };
}());